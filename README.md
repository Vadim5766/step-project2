# Step project 2

### Учасники проекту та завдання
* Дорошенко Вадим
  * Створення проекту на gitlab
  * Створення збірки на gulp
  * Верстка шапки сайту з верхнім меню
  * Верстка секції "People Are Talking About Fork"
* Діденко Мирослав
  * Верстка блоку "Revolutionary Editor"
  * Верстка секції "Here is what you get"
  * Верстка секції "Fork Subscription Pricing"
  * Написання README.md

### Список використаних технологій

Для верстки використовувались:
* html
* scss
* js

Для збірки:
* gulp

Всього було використано 11 пакетів:
* browser-sync
* del
* gulp
* gulp-autoprefixer
* gulp-concat
* gulp-csso
* gulp-imagemin
* gulp-rename
* gulp-sass
* gulp-uglify
* sass











